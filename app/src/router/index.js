import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Home from '@/components/Home'
import Timeline from '@/components/Timeline'
import checkList from '@/components/CheckList'
import Report from '@/components/Report'
import ShowTimeline from '@/components/ShowTimeline'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Home',
      name: 'Home',
      component: Home
    },
    {
      path: '/Timeline',
      name: 'Timeline',
      component: Timeline
    },
    {
      path: '/CheckList',
      name: 'CheckList',
      component: checkList
    },
    {
      path: '/Report',
      name: 'Report',
      component: Report
    },
    {
      path: '/ShowTimeline',
      name: 'ShowTimeline',
      component: ShowTimeline
    }
  ]
})
