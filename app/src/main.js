// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueSession from 'vue-session'
import VueSweetAlert from 'vue-sweetalert'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(VueSession)
Vue.use(VueSweetAlert)
Vue.prototype.$apiUrl = 'https://www.betaskthai.com/vgroupl_api/v1/'
Vue.prototype.$apiUrlHRM = 'https://asia-east2-vgroup-hrm.cloudfunctions.net/'
Vue.prototype.$apiToken = 'YWRiNjBhZTY4NDY4YjYyY2ZhZGU3N2NkMTkxOTMwYTg1YmQ5NDhhOGQ4NDE0N2ZiYzZhNTNkZmIzMzUzZTc4Yw'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
